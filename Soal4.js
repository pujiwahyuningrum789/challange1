// soal no 4
function isValidPassword (givenPassword) {
    const regex = /(?=.*{a-z})(?=.*[0-9]) (?=.*[A-Z])/;
    if (typeof givenPassword === 'string'){
      if (regex.test(givenPassword) && givenPassword.length >= 8) {
        return true;
      }else{
        return false;
      }
    }else if (typeof givenPassword === 'underfined') {
      return 'EROR : Tolong Masukkan Password'
    }else {
      return 'EROR : Invalid Data Type'
    }
  }
  
  console.log(isValidPassword('Meong2021')) // karena memenuhi kriteria, yakni terdiri dari 8 huruf, ada huruf besar, ada huruf kecil, dan ada angka.
  console.log(isValidPassword('meong2021')) // false karena tidak ada huruf besar
  console.log(isValidPassword('@eong')) // false karena hanya terdiri dari 5 huruf
  console.log(isValidPassword('Meong2')) // false karena hanya terdiri dari 6 huruf
  console.log(isValidPassword('0')) // false karena hanya terdiri dari 1 angka sja, tidak memiliki huruf besar maupun huruf kecil
  console.log(isValidPassword('')) // false karena nilai parameter kosong. 
  