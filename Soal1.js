// soal no 1
function changeWord (selectedText, changeText, text){
    if(selectedText[0] === selectedText[0].toUpperCase()){ // cek apakah huruf pertama dari kata yang mau diganti itu huruf besar 
      changeText = changeText[0].toUpperCase() + changeText.slice(1); // ganti huruf pertama dari kata penggantinya huruf besar 
    }else {
      changeText = changeText[0].toLowerCase() + changeText.slice(1); // jika tidak maka huruf pertama dari kata penggantinya ganti dengan huruf kecil
    }
    return text.replaceAll(selectedText,changeText); // replaceAll () adalah modul bawaan js untuk mereplace semua yang sama dengan parameter yang diberikan 
  }
  
  const kalimat1 = 'Andini sangat mencintai kamu selamanya'
  const kalimat2 = 'Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu'
  
  console.log(changeWord('mencintai','membenci',kalimat1))
  
  console.log(changeWord('bromo','semeru',kalimat2))
  