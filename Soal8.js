// soal no 8 
const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hartaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    },
  ];
  
  const currencyFormatting = formatted => {
    return Intl.NumberFormat("id-ID", {
      style: "currency", currency: "IDR"
    }).format(formatted)
  }
  const findMax = arr => {
    let max = Math.max.apply(Math, arr.map(function(o) { return o.totalTerjual; }))
    objIndex = arr.findIndex((obj => obj.totalTerjual == max));
    return objIndex
  }
  const getInfoPenjualan = dataPenjualan => {
    if (typeof dataPenjualan != 'object') {
      return string = "Invalid data type"
    }
    else {
      let totalKeuntungan = dataPenjualan.reduce(function (untungTemp, item) {
        return untungTemp + ((item.hargaJual * item.totalTerjual - item.hargaBeli * (item.totalTerjual - item.sisaStok)));
      }, 0);
      let totalModal = dataPenjualan.reduce(function (modalTemp, item) {
        return modalTemp + (item.hargaBeli * (item.totalTerjual + item.sisaStok));
      }, 0);
      
      
      let persentaseKeuntungan = ((totalKeuntungan / totalModal) * 100).toFixed(0) + "%"
      findMax(dataPenjualan)
      
      return {
        totalKeuntungan: currencyFormatting(totalKeuntungan),
        totalModal: currencyFormatting(totalModal),
        persentaseKeuntungan: persentaseKeuntungan,
        produkBukuTerlaris: dataPenjualan[objIndex].namaProduk,
        penulisTerlaris: dataPenjualan[objIndex].penulis
      }
    }
  }
  
  console.log(getInfoPenjualan(dataPenjualanNovel))
  console.log(getInfoPenjualan())