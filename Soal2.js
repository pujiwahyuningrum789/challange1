// soal no 2
function checkTypeNumber(givenNumber) {
    console.log(typeof givenNumber)
    if (typeof givenNumber == "string" || typeof givenNumber == "undefined"
    || typeof givenNumber == null || typeof givenNumber == 'object') {
      return "Tipe Data Invalid"
    } else if (givenNumber % 2 == 0) {
      return "Genap"
    } else {
      return "Ganjil"
    }
  }
  
  console.log(checkTypeNumber(10))
  console.log(checkTypeNumber(3))
  console.log(checkTypeNumber('3'))
  console.log(checkTypeNumber())